// Select DOM (document object model) Items
// QuerySelector functions Selects only the first element
const menuBtn = document.querySelector(".menu-btn");
const menu = document.querySelector(".menu");
const menuNav = document.querySelector(".menu-nav");
const menuSome = document.querySelector(".some");

// Set the State of Menu
// let is want to directly reassign this, const cannot be used in here
let showMenu = false;

menuBtn.addEventListener("click", toggleMenu);

function toggleMenu() {
  if (!showMenu) {
    // Let's add a class to menuBtn
    menuBtn.classList.add("close");
    menu.classList.add("show");
    menuNav.classList.add("show");
    menuSome.classList.add("show");

    //Set Menu State
    showMenu = true;
  } else {
    // Let's remove selected classes from menuBtn
    menuBtn.classList.remove("close");
    menu.classList.remove("show");
    menuNav.classList.remove("show");
    menuSome.classList.remove("show");
    //Set Menu State
    showMenu = false;
  }
}
